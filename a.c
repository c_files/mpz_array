/*


http://stackoverflow.com/questions/26908628/gmp-mpz-array-init-is-an-obsolete-function-how-should-we-initialize-mpz-arrays

gcc a.c -lgmp


*/
#include <stdlib.h> // malloc
#include <stdio.h>
#include <gmp.h>



int main ()
{       
       int array_size = 100;
       int i;
       mpz_t *array;
      mpz_t temp;


       // allocate memory  for the array
       array = malloc(array_size * sizeof(mpz_t));
       if (NULL == array) {
                       printf("ERROR: Out of memory\n");
                       return 1;
                       }

       
       // init   
        mpz_init(temp);
        for (i = 0; i < array_size; i++) {
          mpz_init2(array[i], 1024);
          }

       // compute 
      // using string and temp variable  
      mpz_set_str (temp, "20023232323232323234343434", 10);
      mpz_set(array[10], temp);
      // using ui
      mpz_set_ui(array[12], 1212121);

      // check
      gmp_printf ("%Zd\n",array[10]); // 
      gmp_printf ("%Zd\n", temp); //
      gmp_printf ("%Zd\n",array[12]); // 

       // free memory 
       for (i = 0; i < array_size; i++) {
        mpz_clear(array[i]);
       }
        
       free(array);
       mpz_clear(temp); 
                
        
        return 0;
}
